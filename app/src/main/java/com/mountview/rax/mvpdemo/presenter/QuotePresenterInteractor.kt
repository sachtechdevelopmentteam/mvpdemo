package com.mountview.rax.mvpdemo.presenter

import com.mountview.rax.mvpdemo.model.QuoteModelInteractor
import com.mountview.rax.mvpdemo.model.interactor.QuoteModel
import com.mountview.rax.mvpdemo.presenter.interactor.QuotePresenter
import com.mountview.rax.mvpdemo.view.interactor.QuoteViewInteractor

/**
 * Created by Akash Saggu(R4X) on 4/10/18 at 13:46.
 * akashsaggu@protonmail.com
 * @Version 1 (4/10/18)
 * @Since on 4/10/18
 */

class QuotePresenterInteractor(private val viewInteractor: QuoteViewInteractor) : QuotePresenter {
    private val quoteModel: QuoteModel by lazy { QuoteModelInteractor() }

    override fun getRandomQuote() {
        val quote = quoteModel.fetchRandomQuote()
        viewInteractor.onQuoteReceived(quote)
    }
}