package com.mountview.rax.mvpdemo.presenter.interactor

/**
 * Created by Akash Saggu(R4X) on 4/10/18 at 14:48.
 * akashsaggu@protonmail.com
 * @Version 1 (4/10/18)
 * @Since on 4/10/18
 */

interface QuotePresenter {
    fun getRandomQuote()
}