package com.mountview.rax.mvpdemo.util

import android.content.Context
import android.os.Handler





inline fun Context.repeat(timeMS: Long, crossinline body: () -> Unit): Handler {
    val handler = Handler()
    var runnable: Runnable? = null
    runnable = Runnable {
        body()
        handler.postDelayed(runnable, timeMS)
    }
    handler.postDelayed(runnable, timeMS)
    return handler
}