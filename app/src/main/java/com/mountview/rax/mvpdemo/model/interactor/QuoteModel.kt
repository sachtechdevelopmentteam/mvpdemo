package com.mountview.rax.mvpdemo.model.interactor

/**
 * Created by Akash Saggu(R4X) on 4/10/18 at 14:49.
 * akashsaggu@protonmail.com
 * @Version 1 (4/10/18)
 * @Since on 4/10/18
 */
interface QuoteModel {
    fun fetchRandomQuote(): String
}