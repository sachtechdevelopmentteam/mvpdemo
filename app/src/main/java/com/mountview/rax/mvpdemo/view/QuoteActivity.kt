package com.mountview.rax.mvpdemo.view

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import com.mountview.rax.mvpdemo.R
import com.mountview.rax.mvpdemo.presenter.QuotePresenterInteractor
import com.mountview.rax.mvpdemo.presenter.interactor.QuotePresenter
import com.mountview.rax.mvpdemo.util.repeat
import com.mountview.rax.mvpdemo.view.interactor.QuoteViewInteractor
import kotlinx.android.synthetic.main.activity_quote.*

class QuoteActivity : AppCompatActivity(), QuoteViewInteractor {
    private val quotePresenter: QuotePresenter by lazy { QuotePresenterInteractor(this) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_quote)
        startFetchingQuotes()
    }

    private fun startFetchingQuotes() {
        applicationContext.repeat(5000) {
            runOnUiThread {
                quotePresenter.getRandomQuote()
            }
        }
    }

    override fun onQuoteReceived(quote: String) {
        quote_text.text = quote
        Log.d("Quote", quote)
    }
}


